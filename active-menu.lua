active_button = {}
fightb =   love.graphics.newImage("images/fightb.png")
activeb =   love.graphics.newImage("images/activeb.png")
function active_button_spawn(x, y, text)
	table.insert(active_button, {x = x, y = y, text = text, id = text, mouseover = false})
end

function active_button_draw()
	for i, v in ipairs(active_button) do
		if v.mouseover == true then
			if active == "Go Fight" then 
				love.graphics.setColor(255, 255, 255) 
				love.graphics.draw(activeb, v.x - 55, v.y - 20)
				love.graphics.setColor(255, 0, 0)
			else
				love.graphics.setColor(255, 255, 255)
				love.graphics.draw(activeb2, v.x - 55, v.y - 20)
				love.graphics.setColor(255, 0, 0)
			end
		else
			if active == "Go Fight" then 
				love.graphics.setColor(255, 255, 255) 
				love.graphics.draw(fightb, v.x - 55, v.y - 20)
			else
				love.graphics.setColor(255, 255, 255)
				love.graphics.draw(activeb, v.x - 55, v.y - 20)
			end
		end
		love.graphics.setFont(activef)
		love.graphics.print(v.text, v.x, v.y)
		love.graphics.setColor(255, 255, 255)
	end
end
function active_button_click(x, y)
	for i, v in ipairs(active_button) do
		if x > v.x and x < v.x + menuf:getWidth(v.text) and y > v.y and y < v.y + menuf:getHeight() then
			if v.text == "Easy" or v.text == "Medium" or v.text == "Hard" then
				obtiznost = v.text
			end
			if v.text == "To town" then
				active = v.text
			end
			if v.text == "Towns" then
				town_active = true
				grind_active = false
				forts_active = false
			end
			if v.text == "Forts" then
				town_active = false
				grind_active = false
				forts_active = true
			end
			if active == "To town" then
				if v.text == "Build" or v.text == "Clear" or v.text == "War" then
					town_active = v.text
				end
				if v.text == "Wall" or v.text == "Church" or v.tex == "Home" or v.text == "Pub" or v.text == "Player" or v.text == "Shop" then
					town_active = v.text
				end
				if v.text == "Get Them" then
					town_active = v.text
				end
			end
			if v.text == "Grind" then
				grind_active = true
				forts_active = false
			end
			if v.text == "Asgard" or v.text == "Taranth" or v.text == "Quintala" then
				town = v.text
			end
			if v.text == "others" then
				others_active = true
			end
			if v.text == "Upgrade" then
				upgrade_active = true
			end
			if v.text == "Go Fight" or v.text == "Capture" then
				active = "Go Fight"
			end
			if v.text == "items" then
				item_active = true
			end
			if v.text == "boss" then
				boss_active = true
			end
			if v.text == "special" then
				boss_active2 = true
			end
			if v.text == "Wardrobe" then
				home_active = true
			end
			if v.text == "swipe" then
				if tonumber(player.hp) <= 0 then
					lose = true
				elseif tonumber(enemy.hp) <= 0 then
					win = true
				else
					hit = true
				end
			end
			if v.text == "Skill 2" then
				if tonumber(player.hp) <= 0 then
					lose = true
				elseif tonumber(enemy.hp) <= 0 then
					win = true
				else
					if player.element == "fire" then
						if tonumber(player.mp) > original_mp * player.lvl / 10 then
							fire_skill = true
						end
					end
					if player.element == "air" then
						if tonumber(enemy.mp) > 0 then
							air_skill = true
						end
					end
					if player.element == "water" then
						if tonumber(player.mp) > original_mp * player.lvl / 10 then
							water_skill = true
						end
					end
					if player.element == "earth" then
						if tonumber(player.mp) > original_mp * player.lvl / 10 then
							earth_skill = true
						end
					end
				end
			end
			if v.text == "Skill 1" then
				if tonumber(player.hp) <= 0 then
					lose = true
				elseif tonumber(enemy.hp) <= 0 then
					win = true
				else
					if player.atrib == "str" then
						if tonumber(player.mp) > original_mp * player.lvl / 4 then
							str_skill = true
						end
					end
					if player.atrib == "agl" then
						if tonumber(player.mp) > 0 then
							agl_skill = true
						end
					end
					if player.atrib == "int" then
						if tonumber(player.mp) > original_mp * player.lvl / 4 then
							int_skill = true
						end
					end
				end
			end
		end
	end
end
function active_button_check()
	for i, v in ipairs(active_button) do
		if mousex > v.x and mousex < v.x + menuf:getWidth(v.text) and mousey > v.y and mousey < v.y + menuf:getHeight() then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end
function active_button_clear()
	for k in pairs (active_button) do
		active_button [k] = nil
	end
end
