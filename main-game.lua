--require "shop"
main_button = {}
active = "Warzone"
if love.filesystem.exists("user.narod") then
	player = {
		narod = love.filesystem.read("user.narod"),
		atrib = love.filesystem.read("user.atrib"),
		element = love.filesystem.read("user.element"),
		push = love.filesystem.read("user.push"),
		sex = love.filesystem.read("user.sex"),
		power = tonumber(string.format("%.1f", love.filesystem.read("user.power"))),
		xp = love.filesystem.read("user.xp"),
		money = love.filesystem.read("user.money"),
		lvl = love.filesystem.read("user.lvl"),
		hp = tonumber(string.format("%.1f", love.filesystem.read("user.hp"))),
		mp = love.filesystem.read("user.mp")
	}
else
	player = {
		narod = "Tenesian",
		atrib = "str",
		element = "fire",
		push = "melee",
		sex = "male",
		power = 0,
		hp = 0,
		mp = 0,
		xp = 0,
		money = 0,
		lvl = 0
	}
end
easy = {
	love.graphics.newImage("images/enemy/easy/1.png"),
	love.graphics.newImage("images/enemy/easy/2.png"),
	love.graphics.newImage("images/enemy/easy/3.png"),
	love.graphics.newImage("images/enemy/easy/4.png"),
	love.graphics.newImage("images/enemy/easy/5.png"),
	love.graphics.newImage("images/enemy/easy/6.png"),
	love.graphics.newImage("images/enemy/easy/7.png"),
	love.graphics.newImage("images/enemy/easy/8.png"),
	love.graphics.newImage("images/enemy/easy/9.png"),
	love.graphics.newImage("images/enemy/easy/10.png"),
	love.graphics.newImage("images/enemy/easy/11.png"),
	love.graphics.newImage("images/enemy/easy/12.png"),
	love.graphics.newImage("images/enemy/easy/13.png")
}
medium = {
	love.graphics.newImage("images/enemy/medium/1.png"),
	love.graphics.newImage("images/enemy/medium/2.png"),
	love.graphics.newImage("images/enemy/medium/3.png"),
	love.graphics.newImage("images/enemy/medium/4.png"),
	love.graphics.newImage("images/enemy/medium/5.png"),
	love.graphics.newImage("images/enemy/medium/6.png"),
	love.graphics.newImage("images/enemy/medium/7.png"),
	love.graphics.newImage("images/enemy/medium/8.png"),
	love.graphics.newImage("images/enemy/medium/9.png"),
	love.graphics.newImage("images/enemy/medium/10.png"),
	love.graphics.newImage("images/enemy/medium/11.png"),
	love.graphics.newImage("images/enemy/medium/12.png")
}
hard = {
	love.graphics.newImage("images/enemy/hard/1.png"),
	love.graphics.newImage("images/enemy/hard/2.png"),
	love.graphics.newImage("images/enemy/hard/3.png"),
	love.graphics.newImage("images/enemy/hard/4.png"),
	love.graphics.newImage("images/enemy/hard/5.png"),
	love.graphics.newImage("images/enemy/hard/6.png"),
	love.graphics.newImage("images/enemy/hard/7.png"),
	love.graphics.newImage("images/enemy/hard/8.png"),
	love.graphics.newImage("images/enemy/hard/9.png"),
	love.graphics.newImage("images/enemy/hard/10.png"),
	love.graphics.newImage("images/enemy/hard/11.png"),
	love.graphics.newImage("images/enemy/hard/12.png")
}
boss = {
	love.graphics.newImage("images/enemy/boss/1.png"),
	love.graphics.newImage("images/enemy/boss/2.png"),
	--love.graphics.newImage("images/enemy/hard/3.png"),
	--love.graphics.newImage("images/enemy/hard/4.png")
}
war = {
	love.graphics.newImage("images/" .. smode .. "/war/1.png"),
	love.graphics.newImage("images/" .. smode .. "/war/2.png"),
	love.graphics.newImage("images/" .. smode .. "/war/3.png"),
	love.graphics.newImage("images/" .. smode .. "/war/4.png")
}
if not love.filesystem.exists("boss_number") then
	love.filesystem.newFile("boss_number")
	boss_number = love.filesystem.write("boss_number", 1)
else
	boss_number = love.filesystem.read("boss_number")
end
if not love.filesystem.exists("boss_win") then
	love.filesystem.newFile("boss_win")
	boss_win = love.filesystem.write("boss_win", 0)
else
	boss_win = love.filesystem.read("boss_win")
end
----------- menu ----------------------
activebg_a = love.graphics.newImage("images//".. smode .."/active.png")
activebg_b = love.graphics.newImage("images/active2.png")
activebg_c = love.graphics.newImage("images/active3.png")

bar = love.graphics.newImage("images/bar.png")
if player.narod == "Tenesian" then border = love.graphics.newImage("images/player/tenesian.png")
elseif player.narod == "Undead" then border = love.graphics.newImage("images/player/undead.png")
elseif player.narod == "Igneans" then border = love.graphics.newImage("images/player/igneans.png")
end
if player.element == "fire" then element = love.graphics.newImage("images/player/fire.png")
elseif player.element == "earth" then element = love.graphics.newImage("images/player/earth.png")
elseif player.element == "air" then element = love.graphics.newImage("images/player/air.png")
elseif player.element == "water" then element = love.graphics.newImage("images/player/water.png")
end
if player.atrib == "str" then player_img = love.graphics.newImage("images/player/str.png") 
elseif player.atrib == "agl" then player_img = love.graphics.newImage("images/player/agl.png") 
elseif player.atrib == "int" then player_img = love.graphics.newImage("images/player/int.png") end


------------- active ------------------
if active == "Home" then
	pokus_text = "home"
else
	pokus_text = "funguje"
end
---------------------------------------

lvl_up = tonumber(player.lvl)
original_hp = player.hp
original_mp = player.mp
original_power = player.power
player.hp = original_hp * player.lvl
player.mp = original_mp * player.lvl
player.power = original_power * player.lvl
lvl_con = false
enemy = {}
enemy_hp = 500
enemy_mp = 100
enemy_power = 30
enemy.power = tonumber(player.power)
enemy_fight = true
hit = false
win = false
lose = false
activebg = activebg_a
require "items"
require "towns"
function main_update(dt)
	if new_player then
		player = {
			narod = love.filesystem.read("user.narod"),
			atrib = love.filesystem.read("user.atrib"),
			element = love.filesystem.read("user.element"),
			push = love.filesystem.read("user.push"),
			sex = love.filesystem.read("user.sex"),
			power = love.filesystem.read("user.power"),
			xp = love.filesystem.read("user.xp"),
			money = love.filesystem.read("user.money"),
			lvl = love.filesystem.read("user.lvl"),
			hp = love.filesystem.read("user.hp"),
			mp = love.filesystem.read("user.mp")}
			if player.narod == "Tenesian" then border = love.graphics.newImage("images/player/tenesian.png")
			elseif player.narod == "Undead" then border = love.graphics.newImage("images/player/undead.png")
			elseif player.narod == "Igneans" then border = love.graphics.newImage("images/player/igneans.png")
			end
			if player.element == "fire" then element = love.graphics.newImage("images/player/fire.png")
			elseif player.element == "earth" then element = love.graphics.newImage("images/player/earth.png")
			elseif player.element == "air" then element = love.graphics.newImage("images/player/air.png")
			elseif player.element == "water" then element = love.graphics.newImage("images/player/water.png")
			end
			if player.atrib == "str" then player_img = love.graphics.newImage("images/player/str.png") 
			elseif player.atrib == "agl" then player_img = love.graphics.newImage("images/player/agl.png") 
			elseif player.atrib == "int" then player_img = love.graphics.newImage("images/player/int.png") end
			original_power = player.power
			original_hp = player.hp
			original_mp = player.mp
			step = 1
			new_player = false
	end
	awidth_min = 250
	awidth_max = love.graphics.getWidth( ) - 100
	aheight_min = 200
	aheight_max = love.graphics.getHeight( ) - 50
	awidth_mid = awidth_max / 2 + 100
	adt = love.graphics.getWidth( )/6
	if tonumber(player.xp) > 5 then player.lvl = 2 end
	if tonumber(player.xp) > 15 then player.lvl = 3 end
	if tonumber(player.xp) > 50 then player.lvl = 4 end
	if tonumber(player.xp) > 100 then player.lvl = 5 end
	if tonumber(player.xp) > 200 then player.lvl = 6 end
	if tonumber(player.xp) > 300 then player.lvl = 7 end
	if tonumber(player.xp) > 400 then player.lvl = 8 end
	if tonumber(player.xp) > 500 then player.lvl = 9 end
	if tonumber(player.xp) > 1000 then player.lvl = 10 end
	if tonumber(player.xp) > 2000 then player.lvl = 11 end
	if tonumber(player.xp) > 3000 then player.lvl = 12 end
	if tonumber(player.xp) > 4000 then player.lvl = 13 end
	if tonumber(player.xp) > 5000 then player.lvl = 14 end
	if tonumber(player.xp) > 10000 then player.lvl = 15 end 
	if tonumber(player.xp) > 20000 then player.lvl = 16 end 
	if tonumber(player.xp) > 30000 then player.lvl = 17 end
	if tonumber(player.xp) > 40000 then player.lvl = 18 end
	if tonumber(player.xp) > 50000 then player.lvl = 19 end
	if tonumber(player.xp) > 100000 then player.lvl = 20 end 
	if lvl_up < tonumber(player.lvl) then lvl_con = true end
	if lvl_con then 
		player.hp = original_hp * player.lvl
		player.mp = original_mp * player.lvl
		player.power = original_power * player.lvl
		lvl_up = lvl_up + 1
		lvl_con = false
		player.money = player.money + 100
	end
	if active == "Go Fight" then
		enemy_fight = false
	else
		enemy_fight = true 
	end
	if enemy_fight then
		if obtiznost == "Easy" then
			enemy.hp = enemy_hp * player.lvl * 2
			enemy.mp = enemy_mp * player.lvl
			enemy.power = enemy_power * player.lvl / 2
			enemy.xp = 1 * player.lvl
			enemy.img = easy[ math.random( #easy ) ]
		end
		if obtiznost == "Medium" then
			enemy.hp = enemy_hp * player.lvl * 3
			enemy.mp = enemy_mp * player.lvl * 0.8
			enemy.power = enemy_power * player.lvl
			enemy.xp = 2 * player.lvl
			enemy.img = medium[ math.random( #medium ) ]
		end
		if obtiznost == "Hard" then
			enemy.hp = enemy_hp * player.lvl * 4
			enemy.mp = enemy_mp * player.lvl * 0.4
			enemy.power = enemy_power * player.lvl * 2
			enemy.xp = 3 * player.lvl
			enemy.img = hard[ math.random( #hard ) ]
		end
		if active == "Boss" then
			enemy.hp = enemy_hp * 200 * boss_number
			enemy.mp = enemy_mp * 2 * boss_number
			enemy.power = enemy_power * player.lvl * 0.15
			enemy.xp = 15 * player.lvl
			enemy.img = boss[ math.random( #boss ) ]
		end
	end
	if hit then
		enemy.miss = math.random(0, 6)
		if enemy.miss >= 1 then			
			if enemy.miss == 3 then
				enemy.hp = enemy.hp - player.power * 1.5
				player_hit = player.power * 1.5
			else
				enemy.hp = enemy.hp - player.power
				player_hit = player.power
			end
		else
			player_hit = 0
		end
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
		end
		hit = false
		--if tonumber(player.hp) <= 0 then
		--	lose = true
		--end
		--if tonumber(enemy.hp) <= 0 then
		--	win = true
		--end
	end
	if str_skill then
		enemy.hp = enemy.hp - player.power * 3
		player_hit = player.power * 3
		player.mp = player.mp - original_mp * player.lvl / 4
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
				player.hp = player.hp + original_hp * player.lvl / 2
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
			player.hp = player.hp + original_hp * player.lvl / 2
		end
		str_skill = false
	end
	if agl_skill then
		enemy.power = enemy.power * 0.6
		player.mp = player.mp - original_mp * player.lvl / 2
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
			--player.mp = player.mp + original_mp * player.lvl / 4
		end
		agl_skill = false
	end
	if int_skill then
		if tonumber(player.hp) < original_hp * player.lvl * 4 then
			player.hp = player.hp + original_hp * player.lvl * 2
		end
		player.mp = player.mp - original_mp * player.lvl / 4
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
				enemy.hp = enemy.hp - player.power * player.lvl
				player_hit = player.power * player.lvl
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
			enemy.hp = enemy.hp - player.power * player.lvl
			player_hit = player.power * player.lvl
		end
		int_skill = false
	end
	if fire_skill then
		player.mp = player.mp - original_mp * player.lvl / 10
		enemy.miss = math.random(0, 6)
		if enemy.miss >= 1 then			
			if enemy.miss == 3 then
				enemy.hp = enemy.hp - player.power * 2
				player_hit = player.power * 1.5
			else
				enemy.hp = enemy.hp - player.power * 1.5
				player_hit = player.power
			end
		else
			enemy.hp = enemy.hp - player.power
		end
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
		end
		fire_skill = false
	end
	if water_skill then		
		if enemy.mp > 0 then
			enemy.mp = enemy.mp - enemy_mp * 0.5
			player.mp = player.mp + original_mp * 0.6
		end	
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 2
				enemy_hit = enemy.power * 2
			else
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			end
		else
			enemy_hit = 0
		end
		water_skill = false
	end
	if air_skill then
		--player.mp = player.mp - original_mp * player.lvl / 10
		enemy.miss = math.random(0, 6)
		if enemy.mp > 0 then
			if enemy.miss >= 1 then			
				if enemy.miss == 3 then
					enemy.hp = enemy.hp - enemy.mp * 4
					enemy.mp = 0
					player_hit = player.power * 1.5
				else
					enemy.hp = enemy.hp - enemy.mp * 3
					enemy.mp = enemy.mp - enemy_mp * 0.5
					player_hit = player.power
				end
			else
				enemy.hp = enemy.hp - enemy.mp * 2
				enemy.mp = enemy.mp - enemy_mp * 0.1
			end
		end
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
		end
		air_skill = false
	end
	if earth_skill then
		player.mp = player.mp - original_mp * player.lvl / 2
		enemy.miss = math.random(0, 6)
		if enemy.miss >= 1 then			
			if enemy.miss == 3 then
				player.power = player.power * 1.8
				--player_hit = player.power * 2
			else
				player.power = player.power * 1.5
				--player_hit = player.power * 1.5
			end
		else
			player.power = player.power * 1.2
			--player_hit = player.power
		end
		enemy.missb = math.random(0, 3)
		if enemy.missb >= 1 then
			if enemy.missb == 3 then
				player.hp = player.hp - enemy.power * 1.5
				enemy_hit = enemy.power * 1.5
			else
				player.hp = player.hp - enemy.power
				enemy_hit = enemy.power
			end
		else
			enemy_hit = 0
		end
		earth_skill = false
	end
	if lose then
		if asg_win < 6 then
			asg_player = asg_player + asg_win
			if tonumber(Asgard.wall) < 1 then
				love.filesystem.write("Asqard.captured", 0)
				Asgard.captured = 0
			else
				Asgard.wall = tonumber(Asgard.wall) - 1
			end
		end
		player.hp = original_hp * player.lvl + item_hp
		player.mp = original_mp * player.lvl + item_mp
		player.power = original_power * player.lvl + item_power
		active = "Warzone"
		if tonumber(player.xp) > 0 then
			if obtiznost == "Medium" then
				player.xp = player.xp - player.lvl
			end
			if obtiznost == "Hard" then
				player.xp = player.xp - player.lvl * 2
			end
		end
		if asg_boss then
			
			asg_boss = false
		end
		if boss_con then
			boss_con = false
		end
		lose = false
		love.filesystem.write("user.xp", player.xp)
		love.filesystem.write("user.money", player.money)
		love.filesystem.write("user.lvl", player.lvl)
		town_active = nil
	end
	if win then
		vyhra = math.random(0,20)
		if asg_win > 6 then
			player.money = player.money + vyhra * (asg_win + 6)
			asg_player = asg_player - asg_win
			asg_win = 6
		end
		if asg_boss then
			love.filesystem.write("Asqard.captured", 1)
			Asgard.captured = 1
			asg_boss = false
		end
		if obtiznost == "Easy" then
			player.money = player.money + vyhra
		end
		if obtiznost == "Medium" then
			player.money = player.money + vyhra * 5
		end
		if obtiznost == "Hard" then
			player.money = player.money + vyhra * 10
			asg_en = true
		end
		if boss_con then
			player.money = player.money + vyhra * 20
			boss_number = boss_number + 1
			boos_con = false
			love.filesystem.write("boss_number", boss_number)
			love.filesystem.write("boss_win", 1)
			boss_win = 1
		end
		player.hp = original_hp * player.lvl + item_hp
		player.mp = original_mp * player.lvl + item_mp
		player.power = original_power * player.lvl + item_power
		player.xp = player.xp + enemy.xp
		active = "Warzone"
		win = false
		love.filesystem.write("user.xp", player.xp)
		love.filesystem.write("user.money", player.money)
		love.filesystem.write("user.lvl", player.lvl)
		town_active = nil
	end
	if active == "Warzone" then
		-- prepracovat na mab styl
		boss_con = false
		war_bg = war[ math.random( #war ) ]
		activebg = activebg_a
		active_text = "Chose where you want to go"
		active_button_clear()
		active_button_spawn(awidth_min, aheight_min + 50, "Towns")
		active_button_spawn(awidth_mid, aheight_min + 50, "Forts")
		active_button_spawn(awidth_max - 50, aheight_min + 50, "Grind")
		if grind_active then 
			town_active = false
			active_button_spawn(awidth_min, aheight_min + 100, "Easy")
			active_button_spawn(awidth_mid, aheight_min + 100, "Medium")
			active_button_spawn(awidth_max - 50, aheight_min + 100, "Hard")
			if obtiznost == "Easy" or obtiznost == "Medium" or obtiznost == "Hard" then
				active_button_spawn(awidth_mid, aheight_max, "Go Fight")
			end
			--grind_active = false
		end
		if town_active then
			--grind_active = false
			item_clear()
			active_button_spawn(awidth_min, aheight_min + 100, "Asgard")
			active_button_spawn(awidth_min, aheight_min + 150, "Taranth")
			active_button_spawn(awidth_min , aheight_min + 200, "Quintala")
			if tonumber(player.lvl) > 9 then
				if town == "Asgard" or town == "Taranth" or town == "Quintala" then
					--town_text = "Will be here soon"
					active_button_spawn(awidth_max  - 50, aheight_max, "To town")
				end
			end
		end
		if tonumber(player.lvl) < 10 then
			if town == "Asgard" or town == "Taranth" or town == "Quintala" then
				town_text = "You can go to town after lvl 10"
			end
		else
			if town == "Asgard" then 
				town_text = "Small town filed with good people. You can see here lot of farms"
			end
			if town == "Taranth" then
				town_text = "Huge army town. Well prepared for defend"
			end
			if town == "Quintala" then
				town_text = "Huge town with churge"
			end
		end
		if fort_active then
			
		end
		--active_button_spawn(300, 700, "Go to Battlezone")
		active_button_check()
	elseif active == "Boss" then
		boss_con = true
		war_bg = war[ math.random( #war ) ]
		activebg = activebg_a
		active_button_clear()
		if tonumber(player.lvl) < 5 then
			active_text = "Boss will be here after lvl 5"
		else
			if tonumber(boss_number) < 6 then
				active_text = "This is boss number " .. boss_number .. " of 6"
				--active_button_clear()
				active_button_spawn(awidth_mid, aheight_max, "Go Fight")
			else
				active_text = "No more boss for you"
			end
		end
		
		--active_button_spawn(300, 700, "Go to Battlezone")
		active_button_check()
	elseif active == "To town" then
		active_text = "Welcome to " .. town
		active_button_clear()
		--towns_clear()
		if town == "Asgard" then 
			if tonumber(Asgard.captured) == 1 then
				asgard()
			else
				asgard_boss()
			end
			--towns_spawn(Asgard)
		end
		if town == "Taranth" then
			--taranth()
			--towns_spawn(Taranth)
		end
		if town == "Quintala" then
			--quintala()
			--towns_spawn(Quintala)
		end	
		--towns_spawn(Taranth)
		--towns_draw()
		
		active_button_check()
	elseif active == "Go Fight" then
		activebg = war_bg
		active_text = "Your battle is here"
		active_button_clear()
		active_button_spawn(awidth_min, aheight_max, "swipe")
		active_button_spawn(awidth_mid, aheight_max, "Skill 1")
		active_button_spawn(awidth_max - 50, aheight_max, "Skill 2")
		--active_button_spawn(1, 1, " ")
		active_button_check()
	elseif active == "Shop" then
		activebg = activebg_c
		active_text = ""
		active_button_clear()
		active_button_spawn(awidth_min, aheight_min - 30, "items")
		active_button_spawn(awidth_mid, aheight_min - 30, "others")
		if tonumber(boss_win) == 1 then
			active_button_spawn(awidth_max - 50, aheight_min - 30, "boss")
		end
		item_check()
		if others_active then
			item_clear()
			item_spawn(upgrade_hp , awidth_min, aheight_min + 20)
			item_spawn(upgrade_mp , awidth_min + adt - 20, aheight_min + 20)
			item_spawn(upgrade_power , awidth_min + (adt - 20) * 2, aheight_min + 20)
			item_spawn(upgrade_hp2 , awidth_min, aheight_min + 150)
			item_spawn(upgrade_mp2,  awidth_min + adt, aheight_min + 150)
			item_spawn(upgrade_power2 , awidth_min + adt * 2, aheight_min + 150)
			item_spawn(upgrade_hp3 , awidth_min + (adt - 20) * 3, aheight_min + 20)
			item_spawn(upgrade_mp3,  awidth_min + adt * 3, aheight_min + 150)
			item_spawn(upgrade_power3 , awidth_min + (adt - 20) * 4, aheight_min + 20)
			others_active = false
		end
		if boss_active then
			item_clear()
			item_spawn(potion_a, awidth_min, aheight_min + 70)
			item_spawn(potion_b, awidth_min + adt, aheight_min + 70)
			item_spawn(potion_c, awidth_min + adt * 2, aheight_min + 70)
			item_spawn(potion_d, awidth_min, aheight_min + 200)
			item_spawn(potion_e, awidth_min + adt, aheight_min + 200)
			item_spawn(potion_f, awidth_min + adt * 2, aheight_min + 200)
			boss_active = false
		end
		if item_active then
			item_clear()
			item_spawn(weapon, awidth_min, aheight_min + 70)
			item_spawn(armor, awidth_min + adt, aheight_min + 70)
			item_spawn(gloves, awidth_min + adt * 2, aheight_min + 70)
			item_spawn(belt, awidth_min + adt * 3, aheight_min + 70)
			item_spawn(braceles, awidth_min, aheight_min + 200)
			item_spawn(pants, awidth_min + adt, aheight_min + 200)
			item_spawn(boots, awidth_min + adt * 2, aheight_min + 200)
			item_spawn(hat, awidth_min + adt * 3, aheight_min + 200)
			item_active = false
		end
		active_button_check()
	elseif active == "Home" then
		activebg = activebg_a
		active_text = " "
		active_button_clear()
		active_button_spawn(awidth_min, aheight_min, "Upgrade")
		active_button_spawn(awidth_mid, aheight_min, "Wardrobe")
		active_button_spawn(awidth_max - 50 , aheight_min, "special")
		item_check()
		if boss_active2 then
			item_clear()
			item_spawn(potion_a, awidth_min, aheight_min + 70)
			item_spawn(potion_b, awidth_min + adt, aheight_min + 70)
			item_spawn(potion_c, awidth_min + adt * 2, aheight_min + 70)
			item_spawn(potion_d, awidth_min, aheight_min + 220)
			item_spawn(potion_e, awidth_min + adt, aheight_min + 220)
			item_spawn(potion_f, awidth_min + adt * 2, aheight_min + 220)
			boss_active2 = false
			--town_active = false
		end
		if home_active then
			item_clear()
			item_spawn(weapon, awidth_min, aheight_min + 70)
			item_spawn(armor, awidth_min + adt, aheight_min + 70)
			item_spawn(gloves, awidth_min + adt * 2, aheight_min + 70)
			item_spawn(belt, awidth_min + adt * 3, aheight_min + 70)
			item_spawn(braceles, awidth_min, aheight_min + 220)
			item_spawn(pants, awidth_min + adt, aheight_min + 220)
			item_spawn(boots, awidth_min + adt * 2, aheight_min + 220)
			item_spawn(hat, awidth_min + adt * 3, aheight_min + 220)
			home_active = false
			--town_active = false
		end
		if upgrade_active then
			item_clear()
			item_spawn(upgrade_hp , awidth_min, aheight_min + 70)
			item_spawn(upgrade_mp , awidth_min + adt - 20, aheight_min + 70)
			item_spawn(upgrade_power , awidth_min + (adt - 20) * 2, aheight_min + 70)
			item_spawn(upgrade_hp2 , awidth_min, aheight_min + 200)
			item_spawn(upgrade_mp2,  awidth_min + adt, aheight_min + 200)
			item_spawn(upgrade_power2 , awidth_min + adt * 2, aheight_min + 200)
			item_spawn(upgrade_hp3 , awidth_min + (adt - 20) * 3, aheight_min + 70)
			item_spawn(upgrade_mp3,  awidth_min + adt * 3, aheight_min + 200)
			item_spawn(upgrade_power3 , awidth_min + (adt - 20) * 4, aheight_min + 70)
			--active_button_spawn(awidth_max - 50, aheight_max, "use it")
			upgrade_active = false
		end
		active_button_check()
	else
		active_button_clear()
	end
end
function main_draw()
	love.graphics.setBackgroundColor( 255, 255, 255)
	love.graphics.draw(activebg, 200, 150)
	love.graphics.setFont(mainf)
	love.graphics.draw(border, 15, 15)
	love.graphics.draw(element, 15, 15)
	love.graphics.draw(player_img, 15, 15)
	love.graphics.draw(bar, 190, 5)
	if player.narod == "Tenesian" then 
		love.graphics.setColor(0, 255, 0)
	elseif player.narod == "Undead" then
		love.graphics.setColor(255, 0, 0)
	elseif player.narod == "Igneans" then 
		love.graphics.setColor(0, 0, 255)
	end
	love.graphics.print("LVL - "..tostring(player.lvl), 400, 50)
	love.graphics.print("XP - "..tostring(player.xp), 600, 50)
	love.graphics.print("HP - "..tostring(original_hp * player.lvl + item_hp), 220, 50)
	love.graphics.print("MP - "..tostring(original_mp * player.lvl + item_mp), 220, 90)
	love.graphics.print("Power - "..tostring(original_power * player.lvl + item_power), 400, 90)
	love.graphics.print("$ - "..tostring(player.money), 600, 90)
	love.graphics.setColor(0, 0, 0)
	love.graphics.setFont(menuf2)
	if active == "Shop" then love.graphics.setColor(255, 255, 255) end
	if active == "Go Fight" then love.graphics.setColor(255, 255, 255) end
	love.graphics.print(active_text, awidth_min, aheight_min)
	active_button_draw()
	if active == "Warzone" then
		if town_active then
			if town == "Asgard" or town == "Taranth" or town == "Quintala" then
				love.graphics.setColor(255,255,255)
				love.graphics.printf(town_text, awidth_mid, aheight_min + 100, awidth_max - awidth_mid, 'left')
			end
		end
		if obtiznost == "Easy" or obtiznost == "Medium" or obtiznost == "Hard" then
			if grind_active then 
				love.graphics.print("Choose your enemy", awidth_mid, aheight_min + adt +75)
				love.graphics.print("HP - "..tostring(enemy.hp), awidth_mid, aheight_min + adt +100)
				love.graphics.print("MP - "..tostring(enemy.mp), awidth_mid, aheight_min + adt + 125)
				love.graphics.print("power - "..tostring(enemy.power), awidth_mid, aheight_min + adt + 150)
				love.graphics.setColor(255,255,255)
			end
			--love.graphics.draw(enemy.img, 300, 400)
		end
	elseif active =="Go Fight" then
		
	else
		obtiznost = nil
	end
	if active == "Boss" then
		if tonumber(player.lvl) >= 5 then
			if tonumber(boss_number) < 6 then
				--love.graphics.print("Choose your enemy", 500, 350)
				love.graphics.print("HP - "..tostring(enemy.hp), awidth_mid, aheight_min + adt)
				love.graphics.print("MP - "..tostring(enemy.mp), awidth_mid, aheight_min + adt + 50)
				love.graphics.print("power - "..tostring(enemy.power), awidth_mid, aheight_min + adt + 100)
				love.graphics.setColor(255,255,255)	
			end
		end
	end
	if active == "Go Fight" then
		love.graphics.setBackgroundColor( 0, 0, 0)
		love.graphics.setColor(255,255,255)
		love.graphics.draw(player_img, awidth_min - 40, aheight_min + adt * 2 - 120)
		love.graphics.draw(enemy.img, awidth_max - adt, aheight_min + adt * 2 - 80)
		--love.graphics.setColor(0,0,0)
		love.graphics.setFont(menuf2)
		love.graphics.draw(fightb2, awidth_min - 50, aheight_min + adt - 60)
		love.graphics.draw(fightb2, awidth_max - adt - 50, aheight_min + adt - 60)
		love.graphics.print("HP - "..tostring(player.hp), awidth_min, aheight_min + adt - 30)
		love.graphics.print("HP - "..tostring(enemy.hp), awidth_max - adt, aheight_min + adt - 30)
		love.graphics.print("MP - "..tostring(player.mp), awidth_min, aheight_min + adt + 5)
		love.graphics.print("MP - "..tostring(enemy.mp), awidth_max - adt, aheight_min + adt + 5)
		love.graphics.setFont(activef)
		
		if player_hit == nil then
		else
			love.graphics.print("your dmg in last hit "..tostring(player_hit), awidth_mid - 100, aheight_min + adt/2)
		end
		if enemy_hit == nil then
		else
			love.graphics.print("enemy dmg in last hit "..tostring(enemy_hit), awidth_mid - 100, aheight_max - adt/2)
		end 
		
	end
	---- vytvorit dalsi tlacitka ----
	--end
	love.graphics.setColor(255, 255, 255)
	if active == "Shop" then
		love.graphics.setFont(font)
		item_draw()
	end
	if active == "Home" then
		love.graphics.setFont(font)
		item_draw()
	end
	if active == "To town" then
		if town == "Asgard" then 
			if tonumber(Asgard.captured) == 1 then
				asgard()
			else
				asgard_boss()
			end
			--towns_spawn(Asgard)
		end
		if town == "Taranth" then
			--taranth()
			--towns_spawn(Taranth)
		end
		if town == "Quintala" then
			--quintala()
			--towns_spawn(Quintala)
		end	
		--towns_spawn(Taranth)
		--towns_draw()
		--towns_draw()
	end
end

function main_keys(key)
	
end
