function create_item(t)
	assert(type(t) == "table", "Table expected. Got " .. type(t) ..  " instead.")

	t.name = t.name or "nil"
	t.category = (type(t.category) == "table" and #t.category ~=0) and t.category or { "nil" }
	t.display_file = t.display_file or "images/items/nil.png"

	return t
end

-------------------------------------------------------------------------------

sword_1 = create_item{
	name = "Test sword 1",
	damage = 10,
	durability = 20,
	weight = 5,
	price = 100,
	category = {
		"weapon", "sword"
	},
	display_file = "images/items/weapon/sword_1.png",
}

sword_2 = create_item{
	name = "Test sword 2",
	damage = 15,
	durability = 25,
	weight = 5,
	price = 100,
	--display_file = "images/items/weapon/sword_1.png",
}

sword_3 = create_item{
	name = "Test sword 3",
	damage = 20,
	durability = 30,
	weight = 7,
	price = 100,
	category = {
		"weapon", "sword"
	},
	display_file = "images/items/weapon/sword_3.png",
}

breastplate_1 = create_item{
	name = "Shirt",
	defense = 5,
	durability = 20,
	weight = 10,
	price = 100,
	category = {
		"armor", "shirt"
	},
	display_file = "images/items/armor/breast_1.png",
}

leggings_1 = create_item{
	name = "Leggings1",
	defense = 5,
	durability = 20,
	weight = 10,
	price = 100,
	category = {
		"armor", "pants"
	},
	display_file = "images/items/armor/leggs_1.png",
}

gloves_1 = create_item{
	name = "Gloves",
	defense = 5,
	durability = 20,
	weight = 10,
	price = 100,
	category = {
		"armor", "gloves"
	},
	display_file = "images/items/armor/gloves_1.png",
}

amulet_1 = create_item{
	name = "Amulet 1",
	damage = 5,
	defense = 5,
	weight = 3,
	price = 100,
	category = {
		"jewelry", "necklace"
	},
	display_file = "images/items/jewelry/amulet_1.png",
}

ring_1 = create_item{
	name = "Ring 1",
	weight = 1,
	price = 100,
	category = {
		"jewelry", "ring"
	},
	display_file = "images/items/jewelry/ring_1.png",
}

earring_1 = create_item{
	name = "Earring 1",
	hp = 40,
	weight = 1,
	price = 100,
	category = {
		"jewelry", "earring"
	},
	display_file = "images/items/jewelry/ring_1.png",
}

bracelet_1 = create_item{
	name = "Bracelet 1",
	hp = 10,
	mp = 60,
	weight = 1,
	price = 100,
	category = {
		"jewelry", "necklace"
	},
	display_file = "images/items/jewelry/bracelet_1.png",
}

-------------------------------------------------------------------------------

shop_font = activef2

category_horizontal_size = 250
item_display_size = 200
attribute_text_height = shop_font:getHeight() + 2;

function place_buy_buttons(x, y, shop)
	local k = 0
	for category_name, category in pairs(shop) do
		for i, item in ipairs(category) do
			active_button_spawn(x + category_horizontal_size * k, 
								y + item_display_size * i - 40, 
								"BUY")
		end
		k = k + 1
	end
	active_button_check()
end

function display_shop(x, y, shop)
	local i = 0

	love.graphics.setFont(shop_font)

	for position, category in pairs(shop) do
		display_item_category(x + category_horizontal_size * i, y, category)

		i = i + 1
	end
end

function display_item_category(x, y, category)
	local i = 0

	for position, item in ipairs(category) do
		display_item(x, y + item_display_size * i, item)

		i = i + 1
	end
end

function display_item(x, y, item)
	-- display icon
	if (item.display_image == nil) then
		item.display_image = love.graphics.newImage(item.display_file)
	else
		love.graphics.draw(item.display_image, x, y)
	end

	-- display title
	love.graphics.printf(item.name, 
		                 x,
		                 y,
			             item_display_size, 
		                 'center')

	-- display attributes, lazy mode. 
	local i = 1
	for attribute, value in pairs(item) do
		if (attribute ~= "display_file" 
				and attribute ~= "display_image"  
				and attribute ~= "name"
				and attribute ~= "price"
				and attribute ~= "category") then
			love.graphics.printf(attribute .. ": " .. value, 
				                 x, 
				                 y + attribute_text_height * i,
 				                 item_display_size, 
				                 'left')
			i = i + 1
	    end
	end	

	-- display category
	love.graphics.printf("category" .. ": " .. item.category[1], 
		                 x, 
		                 y + attribute_text_height * i,
			             item_display_size, 
		                 'left')

	-- display price
	love.graphics.printf(item.price .. " moneys!", 
		                 x,
		                 y + item_display_size - attribute_text_height,
			             item_display_size, 
		                 'right')
end

-------------------------------------------------------------------------------

shop_list_1 = {
	weapons = {
		sword_1,
		sword_2,
	},

	armor = {
		breastplate_1,
		gloves_1,
	},

	jewelry = {
		bracelet_1,
		ring_1,
	},
}