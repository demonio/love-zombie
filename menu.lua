button = {}
button_gui =   love.graphics.newImage("images/button.png")
menub =   love.graphics.newImage("images/menub.png")
function button_spawn(x, y, text)
	table.insert(button, {x = x, y = y, text = text, id = text, mouseover = false})
end

function button_draw()
	for i, v in ipairs(button) do
		
		if gamestate == "menu" then
			menuf = menuf3
			love.graphics.setFont(menuf)
			love.graphics.draw(menub, v.x - 25, v.y - 15)
		elseif gamestate == "New game" then
			menuf = menuf3
			love.graphics.setFont(menuf)
		elseif gamestate == "Options" then
			menuf = menuf2
			love.graphics.setFont(menuf)
		elseif gamestate == "Continue" then
			menuf = menuf2
			if active == v.text then
				if player.narod == "Tenesian" then 
					love.graphics.setColor(0, 255, 0)
				elseif player.narod == "Undead" then
					love.graphics.setColor(255, 0, 0)
				elseif player.narod == "Igneans" then 
					love.graphics.setColor(0, 0, 255)
				end
			end
			love.graphics.draw(button_gui, v.x - 55, v.y - 20)
			love.graphics.setFont(menuf)
		end
		if v.mouseover == true then
			if gamestate == "Continue" then
				love.graphics.setColor(255, 255, 255)
				love.graphics.draw(activeb2, v.x - 55, v.y - 20)
				love.graphics.setColor(255, 0, 0)
			else
				love.graphics.setColor(255, 0, 0)
			end
		else
			love.graphics.setColor(255, 255, 255)
		end
		love.graphics.print( v.text, v.x, v.y)
		love.graphics.setColor(255, 255, 255)
	end
end
function button_click(x, y)
	for i, v in ipairs(button) do
		if x > v.x and x < v.x + menuf:getWidth(v.text) and y > v.y and y < v.y + menuf:getHeight() then
			if gamestate == "menu" then
				gamestate = v.text
			elseif gamestate == "Story" then
				if v.text =="Next" then
					step = step + 1
				end
			elseif gamestate == "New game" then
				if v.text == "Tenesian" or v.text == "Undead" or v.text == "Igneans" then
					user.narod = v.text
					step = 2
				elseif v.text == "str" or v.text == "int" or v.text == "agl" then
					user.atrib = v.text
					step = 3
				elseif v.text == "water" or v.text == "fire" or v.text == "air" or v.text == "earth" then
					user.element = v.text
					step = 4
				elseif v.text == "melee" or v.text == "range" then
					user.push = v.text
					step = 5
				elseif v.text == "male" or v.text == "female" then
					user.sex = v.text
					step = 6
				end
			elseif gamestate == "Continue" then
				active = v.text
				if active == "Warzone" then
					active_text = "Pick your enemy or go into war"
				else
					active_text = "Comming soon"
				end
				if v.text == "Quit" then gamestate = "Quit"
					---- ukladani pozic pri odchodu ----
				end
			elseif gamestate == "Options" then
				if v.text == "Fullscreen" then
					if tonumber(fulls) == 0 then
						fulls = 1
						screen = true
					elseif tonumber(fulls) == 1 then
						fulls = 0
						screen = true
					end
					print(fulls)
					love.filesystem.write("screen.full", fulls)
				elseif v.text == "800x600" then
					swidth = 800
					sheight = 600
					smode = "a"
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					love.filesystem.write("screen.smode", smode)
					screen = true
				elseif v.text == "1024x768" then
					swidth = 1024
					sheight = 768
					smode = "a"
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					love.filesystem.write("screen.smode", smode)
					screen = true
				elseif v.text == "1360x768" then
					swidth = 1360
					sheight = 768
					smode = "b"
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					love.filesystem.write("screen.smode", smode)
					screen = true
				elseif v.text == "1366x768" then
					swidth = 1366
					sheight = 768
					smode = "b"
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					love.filesystem.write("screen.smode", smode)
					screen = true
				elseif v.text == "1680x1050" then
					swidth = 1650
					sheight = 1050
					smode = "c"
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					screen = true
				elseif v.text == "1920x1080" then
					swidth = 1920
					sheight = 1080
					love.filesystem.write("screen.width", swidth)
					love.filesystem.write("screen.height", sheight)
					smode = "c"
					love.filesystem.write("screen.smode", smode)
					screen = true
				end
			end
		end
	end
end
function button_check()
	for i, v in ipairs(button) do
		if mousex > v.x and mousex < v.x + menuf:getWidth(v.text) and mousey > v.y and mousey < v.y + menuf:getHeight() then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end
function button_clear()
	for k in pairs (button) do
		button [k] = nil
	end
end
