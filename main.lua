if love.filesystem.exists("screen.width") then
	swidth = love.filesystem.read("screen.width")
	sheight = love.filesystem.read("screen.height")
	fulls = love.filesystem.read("screen.full")
	smode = love.filesystem.read("screen.smode")
	--lang = love.filesystem.read("game.lang")
else
	swidth = 800
	sheight = 600
	fulls = 0
	smode = "a"
	love.filesystem.newFile("screen.width")
	love.filesystem.write("screen.width", swidth)
	love.filesystem.newFile("screen.height")
	love.filesystem.write("screen.height", sheight)
	love.filesystem.newFile("screen.full")
	love.filesystem.write("screen.full", fulls)
	love.filesystem.newFile("screen.smode")
	love.filesystem.write("screen.smode", smode)
end
activeb2 =   love.graphics.newImage("images/activeb2.png")
screen = true
menuf = love.graphics.newFont("fonts/menu.ttf", love.graphics.getHeight( )/10)
menuf3 = love.graphics.newFont("fonts/menu.ttf", love.graphics.getHeight( )/10)
menuf2 = love.graphics.newFont("fonts/menu.ttf", love.graphics.getHeight( )/20)
gamef = love.graphics.newFont("fonts/menu.ttf", love.graphics.getHeight( )/25)
gamef2 = love.graphics.newFont("fonts/menu.ttf", love.graphics.getHeight( )/30)
mainf = love.graphics.newFont("fonts/main.ttf", love.graphics.getHeight( )/30)
mainf2 = love.graphics.newFont("fonts/main.ttf", love.graphics.getHeight( )/32)
activef = love.graphics.newFont("fonts/active.ttf", love.graphics.getHeight( )/20)
activef2 = love.graphics.newFont("fonts/active.ttf", love.graphics.getHeight( )/30)
font = love.graphics.newFont(15)
require "menu"
require "credits"
require "controls"
require "game"
require "main-game"
require "active-menu"
require "text/en"
mbg =   love.graphics.newImage("images/bkg.png")
ngbg =  love.graphics.newImage("images/ngbg.png")
gbg =   love.graphics.newImage("images/gbg.png")
fightb2 =   love.graphics.newImage("images/fightb2.png")
--require "npc"


do
    -- will hold the currently playing sources
    local sources = {}

    -- check for sources that finished playing and remove them
    -- add to love.update
    function love.audio.update()
        local remove = {}
        for _,s in pairs(sources) do
            if s:isStopped() then
                remove[#remove + 1] = s
            end
        end

        for i,s in ipairs(remove) do
            sources[s] = nil
        end
    end

    -- overwrite love.audio.play to create and register source if needed
    local play = love.audio.play
    function love.audio.play(what, how, loop)
        local src = what
        if type(what) ~= "userdata" or not what:typeOf("Source") then
            src = love.audio.newSource(what, how)
            src:setLooping(loop or false)
        end

        play(src)
        sources[src] = src
        return src
    end

    -- stops a source
    local stop = love.audio.stop
    function love.audio.stop(src)
        if not src then return end
        stop(src)
        sources[src] = nil
    end
end
new_game = true
line_height = 0
step = 1
function love.load()
	gamestate = "menu"
	if gamestate == "menu" then
		love.audio.play("music/menu.mp3", "stream", true)
		love.audio.setVolume(0.1)
	end
	if gamestate == "New game" then
		--game_load()
		--button_spawn(600, 200, "Pokus")
	end
	if gamestate == "Continue" then
	end
end
love_update_delta = 0
function love.update(dt)
	--print(smode)
	local love_timer_sleep = love.timer.sleep -- in s (like >=0.8.0)
	if love._version:find("^0%.[0-7]%.") then -- if version < 0.8.0
		-- love.timer.sleep in ms
		love_timer_sleep = function(s) love.timer.sleep(s*1000) end
	end
	if dt < 1/15 then
		love_timer_sleep(1/15 - dt)
	end
	if tonumber(fulls) == 0 then
		full = false
	elseif tonumber(fulls) == 1 then
		full = true
	end
	if screen then
		love.window.setMode( swidth, sheight, {fullscreen=full})
		screen = false
	end	
	love_update_delta = dt
	mousex = love.mouse.getX()
	mousey = love.mouse.getY()
	if gamestate == "menu" then
		button_clear()
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7, "New game")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + 80, "Continue")
		--button_spawn(600, 300, "Options")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + 160, "Controls")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + 240, "Options")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + 320, "Credits")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + 400, "Quit")
		button_check()
	end
	if gamestate == "Options" then
		button_clear()
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 - menuf:getHeight(), "Fullscreen")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7, "800x600")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + menuf:getHeight()*1, "1024x768")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + menuf:getHeight()*2, "1360x768")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + menuf:getHeight()*3, "1366x768")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + menuf:getHeight()*4, "1680x1050")
		button_spawn(love.graphics.getWidth( )/3.2, love.graphics.getHeight( )/7 + menuf:getHeight()*5, "1920x1080")
		button_check()
	end
	if gamestate == "New game" then
		if step == 1 then
			newgame_text = "Choose your Nation"
			button_clear()
			init_x, init_y = 50, love.graphics.getWidth( )/4
			line_height = menuf:getHeight() + 60
			button_spawn(init_x, init_y, "Tenesian")
			button_spawn(init_x, init_y + line_height, "Undead")
			button_spawn(init_x, init_y + line_height * 2, "Igneans")
		end
		if step == 2 then
			newgame_text = "Choose your atribute"
			button_clear()
			init_x, init_y = 100, 220
			line_height = menuf:getHeight() + 60
			button_spawn(init_x, init_y, "str")
			button_spawn(init_x, init_y + line_height, "int")
			button_spawn(init_x, init_y + line_height * 2, "agl")
		end
		if step == 3 then
			newgame_text = "Choose your element"
			button_clear()
			init_x, init_y = 100, 220
			line_height = menuf:getHeight() + 30
			button_spawn(init_x, init_y, "water")	
			button_spawn(init_x, init_y + line_height, "fire")
			button_spawn(init_x, init_y + line_height * 2, "air")
			button_spawn(init_x, init_y + line_height * 3, "earth")
		end
		if step == 4 then
			newgame_text = "Choose your fight style"
			button_clear()
			init_x, init_y = 100, 320
			line_height = menuf:getHeight() + 100
			button_spawn(init_x, init_y, "melee")
			button_spawn(init_x, init_y + line_height, "range")
		end
		if step == 5 then
			newgame_text = "Choose if you are male or female"
			button_clear()
			init_x, init_y = 100, 320
			line_height = menuf:getHeight() + 100
			button_spawn(init_x, init_y, "male")
			button_spawn(init_x, init_y + line_height, "female")
		end
		if step == 6 then
					if user.atrib == "str" then
						user.hp = user.hp * 2
						user.mp = user.mp / 2
					elseif user.atrib == "int" then
						user.hp = user.hp / 2
						user.mp = user.mp * 2
					elseif user.atrib == "agl" then
						user.hp = user.hp 
						user.mp = user.mp
						user.power = user.power * 2
					end
					if user.push == "melee" then
						user.hp = user.hp * 1.2
						user.power = user.power / 1.2
					end
					if user.push == "range" then
						user.hp = user.hp / 1.2
						user.power = user.power * 1.2
					end
					if user.sex == "male" then
						user.hp = user.hp * 1.2
						user.mp = user.mp / 1.2
					elseif user.sex =="female" then
						user.hp = user.hp / 1.2
						user.mp = user.mp * 1.2
					end
					love.filesystem.newFile("user.narod")
					love.filesystem.write("user.narod", user.narod)
					love.filesystem.newFile("user.atrib")
					love.filesystem.write("user.atrib", user.atrib)
					love.filesystem.newFile("user.element")
					love.filesystem.write("user.element", user.element)
					love.filesystem.newFile("user.push")
					love.filesystem.write("user.push", user.push)
					love.filesystem.newFile("user.sex")
					love.filesystem.write("user.sex", user.sex)
					love.filesystem.newFile("user.power")
					love.filesystem.write("user.power", user.power)
					love.filesystem.newFile("user.hp")
					love.filesystem.write("user.hp", user.hp)
					love.filesystem.newFile("user.mp")
					love.filesystem.write("user.mp", user.mp)
					love.filesystem.newFile("user.xp")
					love.filesystem.write("user.xp", user.xp)
					love.filesystem.newFile("user.money")
					love.filesystem.write("user.money", user.xp)
					love.filesystem.newFile("user.lvl")
					love.filesystem.write("user.lvl", user.lvl)
					player = {
						narod = love.filesystem.read("user.narod"),
						atrib = love.filesystem.read("user.atrib"),
						element = love.filesystem.read("user.element"),
						push = love.filesystem.read("user.push"),
						sex = love.filesystem.read("user.sex"),
						power = love.filesystem.read("user.power"),
						xp = love.filesystem.read("user.xp"),
						money = love.filesystem.read("user.money"),
						lvl = love.filesystem.read("user.lvl"),
						hp = love.filesystem.read("user.hp"),
						mp = love.filesystem.read("user.mp")
					}
					love.filesystem.write("weapon.sold", 0)
					love.filesystem.write("armor.sold", 0)
					love.filesystem.write("gloves.sold", 0)
					love.filesystem.write("belt.sold", 0)
					love.filesystem.write("braceles.sold", 0)
					love.filesystem.write("pants.sold", 0)
					love.filesystem.write("boots.sold", 0)
					love.filesystem.write("hat.sold", 0)
					love.filesystem.write("potion_a.sold", 0)
					love.filesystem.write("potion_b.sold", 0)
					love.filesystem.write("potion_c.sold", 0)
					love.filesystem.write("potion_d.sold", 0)
					love.filesystem.write("potion_e.sold", 0)
					love.filesystem.write("potion_f.sold", 0)
					love.filesystem.write("Asqard.captured", 0)
					love.filesystem.write("Taranth.captured", 0)
					love.filesystem.write("Quintala.captured", 0)
					love.filesystem.write("Asqard.wall", 0)
					love.filesystem.write("Taranth.wall", 0)
					love.filesystem.write("Quintala.wall", 0)
					love.filesystem.write("Asqard.church", 0)
					love.filesystem.write("Taranth.church", 0)
					love.filesystem.write("Quintala.church", 0)
					love.filesystem.write("Asqard.home", 0)
					love.filesystem.write("Taranth.home", 0)
					love.filesystem.write("Quintala.home", 0)
					love.filesystem.write("Asqard.pub", 0)
					love.filesystem.write("Taranth.pub", 0)
					love.filesystem.write("Quintala.pub", 0)
					love.filesystem.write("Asqard.player", 0)
					love.filesystem.write("Taranth.player", 0)
					love.filesystem.write("Quintala.player", 0)
					love.filesystem.write("Asqard.shop", 0)
					love.filesystem.write("Taranth.shop", 0)
					love.filesystem.write("Quintala.shop", 0)
					love.filesystem.write("boss_number", 1)
					love.filesystem.write("potion_a.upgrade", 0)
					love.filesystem.write("potion_b.upgrade", 0)
					love.filesystem.write("potion_c.upgrade", 0)
					love.filesystem.write("potion_d.upgrade", 0)
					love.filesystem.write("potion_e.upgrade", 0)
					love.filesystem.write("potion_f.upgrade", 0)
					love.filesystem.write("upgrade_hp.sold", 0)
					love.filesystem.write("upgrade_mp.sold", 0)
					love.filesystem.write("upgrade_power.sold", 0)
					love.filesystem.write("upgrade_hp2.sold", 0)
					love.filesystem.write("upgrade_mp2.sold", 0)
					love.filesystem.write("upgrade_power2.sold", 0)
					love.filesystem.write("upgrade_hp3.sold", 0)
					love.filesystem.write("upgrade_mp3.sold", 0)
					love.filesystem.write("upgrade_power3.sold", 0)
					step = 1
					gamestate = "Story"
					new_player = true
		end
	
		--button_spawn(900, 600, "NEXT")
	
		button_spawn(1, 1, " ")
		button_check()
		game_update(dt)
	end
	if gamestate == "Story" then
		if step == 1 then
			button_clear()
			button_spawn(love.graphics.getWidth( ) - 100, love.graphics.getHeight( ) -50, "Next")
			button_check()
		elseif step == 2 then
			button_clear()
			button_spawn(love.graphics.getWidth( )- 100, love.graphics.getHeight( ) -50, "Next")
			button_check()
		else
			gamestate = "Continue"
		end
	end
	if gamestate == "Quit" then
		if love.filesystem.exists("user.narod") then
			love.filesystem.write("user.xp", player.xp)
			love.filesystem.write("user.money", player.money)
			love.filesystem.write("user.lvl", player.lvl)
		end
		love.event.push('quit')
	end
	if gamestate == "Continue" then
		button_clear()
		button_spawn(50, love.graphics.getHeight( )/2, "Home")
		button_spawn(50, love.graphics.getHeight( )/2 + love.graphics.getHeight( )/10, "Boss")
		button_spawn(50, love.graphics.getHeight( )/2 + love.graphics.getHeight( )/10 * 2, "Warzone")
		button_spawn(50, love.graphics.getHeight( )/2 + love.graphics.getHeight( )/10 * 3, "Shop")
		button_spawn(50, love.graphics.getHeight( )/2 + love.graphics.getHeight( )/10 * 4, "Quit")
		button_check()
		main_update(dt)
	end
end
function love.draw()
	love.graphics.setFont(menuf2)
	if gamestate == "Story" then
		button_draw()
		if step == 1 then 
			love.graphics.printf(story_a, 50, 50, love.graphics.getWidth( ) - 100, 'left')
		elseif step == 2 then
			love.graphics.printf(story_b, 50, 50, love.graphics.getWidth( ) - 100, 'left')
		end
	end
	if gamestate == "menu" then
		love.graphics.draw(mbg, 0, 0)
		if mbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(mbg, mbg:getWidth(), 0)
		end
		if mbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(mbg, 0, mbg:getHeight())
			love.graphics.draw(mbg, mbg:getWidth(), mbg:getHeight())
		end
		button_draw()
	end
	if gamestate == "Options" then
		love.graphics.draw(mbg, 0, 0)
		if mbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(mbg, mbg:getWidth(), 0)
		end
		if mbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(mbg, 0, mbg:getHeight())
			love.graphics.draw(mbg, mbg:getWidth(), mbg:getHeight())
		end
		button_draw()
	end
	if gamestate == "Credits" then
		love.graphics.draw(mbg, 0, 0)
		if mbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(mbg, mbg:getWidth(), 0)
		end
		if mbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(mbg, 0, mbg:getHeight())
			love.graphics.draw(mbg, mbg:getWidth(), mbg:getHeight())
		end
		credits_text()
	end
	if gamestate == "Controls" then
		love.graphics.draw(mbg, 0, 0)
		if mbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(mbg, mbg:getWidth(), 0)
		end
		if mbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(mbg, 0, mbg:getHeight())
			love.graphics.draw(mbg, mbg:getWidth(), mbg:getHeight())
		end
		controls_text()
	end
	if gamestate == "New game" then
		love.graphics.draw(ngbg, 0, 0)
		if ngbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(ngbg, ngbg:getWidth(), 0)
		end
		if ngbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(ngbg, 0, ngbg:getHeight())
			love.graphics.draw(ngbg, ngbg:getWidth(), ngbg:getHeight())
		end
		--game_text()
		game_draw()
		button_draw()
	end
	if gamestate == "Continue" then
		love.graphics.draw(gbg, 0, 0)
		if gbg:getWidth() < love.graphics.getWidth( ) then
			love.graphics.draw(gbg, gbg:getWidth(), 0)
		end
		if gbg:getHeight() < love.graphics.getHeight( ) then
			love.graphics.draw(gbg, 0, gbg:getHeight())
			love.graphics.draw(gbg, gbg:getWidth(), gbg:getHeight())
		end
		main_draw()
		button_draw()
	end
	love.graphics.setFont(activef)
	--love.graphics.printf("FPS: " .. tostring(love.timer.getFPS()), 0, 0, love.graphics.getWidth( ) - 10, "right")
	--love.graphics.printf("dt: " .. string.format("%.4f", love_update_delta), 0, activef:getHeight(), love.graphics.getWidth( ) - 10, "right")
end
function love.mousepressed(x, y, button)
	if button == "l" then
		if gamestate == "Story" then
			button_click(x, y)
		end
		if gamestate == "menu" then
			button_click(x, y)
		end
		if gamestate == "Options" then
			button_click(x, y)
		end
		if gamestate == "New game" then
			--game_click(x, y)
			button_click(x, y)
		end
		if gamestate == "Continue" then
			button_click(x, y)
			active_button_click(x, y)
			item_click(x, y)
		end
	end
end
function love.keypressed(key)
	if gamestate == "menu" then
		if key == "escape" then
			gamestate = "Quit"
		end
	else
		if key == "escape" then
			if town_active == "Wall" or town_active == "Church" or town_active == "Home" or town_active == "Pub" or town_active == "Player" or town_active == "Shop" then
				town_active = "Build"
			else
				gamestate = "menu"
			end
		end
	end
	if gamestate == "New game" then
		game_keys(key)
	end
	if gamestate == "Continue" then
		main_keys(key)
	end
end
