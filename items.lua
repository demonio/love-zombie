item = {}
if not love.filesystem.exists("weapon.sold") then
	love.filesystem.newFile("weapon.sold")
	love.filesystem.newFile("armor.sold")
	love.filesystem.newFile("gloves.sold")
	love.filesystem.newFile("belt.sold")
	love.filesystem.newFile("braceles.sold")
	love.filesystem.newFile("pants.sold")
	love.filesystem.newFile("boots.sold")
	love.filesystem.newFile("hat.sold")
	love.filesystem.write("weapon.sold", 0)
	love.filesystem.write("armor.sold", 0)
	love.filesystem.write("gloves.sold", 0)
	love.filesystem.write("belt.sold", 0)
	love.filesystem.write("braceles.sold", 0)
	love.filesystem.write("pants.sold", 0)
	love.filesystem.write("boots.sold", 0)
	love.filesystem.write("hat.sold", 0)
end
if not love.filesystem.exists("potion_f.sold") then
	love.filesystem.newFile("potion_a.sold")
	love.filesystem.newFile("potion_b.sold")
	love.filesystem.newFile("potion_c.sold")
	love.filesystem.newFile("potion_d.sold")
	love.filesystem.newFile("potion_e.sold")
	love.filesystem.newFile("potion_f.sold")
	love.filesystem.write("potion_a.sold", 0)
	love.filesystem.write("potion_b.sold", 0)
	love.filesystem.write("potion_c.sold", 0)
	love.filesystem.write("potion_d.sold", 0)
	love.filesystem.write("potion_e.sold", 0)
	love.filesystem.write("potion_f.sold", 0)
end
if not love.filesystem.exists("upgrade_hp.sold") then
	love.filesystem.newFile("upgrade_hp.sold")
	love.filesystem.newFile("upgrade_mp.sold")
	love.filesystem.newFile("upgrade_power.sold")
	love.filesystem.newFile("upgrade_hp2.sold")
	love.filesystem.newFile("upgrade_mp2.sold")
	love.filesystem.newFile("upgrade_power2.sold")
	love.filesystem.newFile("upgrade_hp3.sold")
	love.filesystem.newFile("upgrade_mp3.sold")
	love.filesystem.newFile("upgrade_power3.sold")
	love.filesystem.write("upgrade_hp.sold", 0)
	love.filesystem.write("upgrade_mp.sold", 0)
	love.filesystem.write("upgrade_power.sold", 0)
	love.filesystem.write("upgrade_hp2.sold", 0)
	love.filesystem.write("upgrade_mp2.sold", 0)
	love.filesystem.write("upgrade_power2.sold", 0)
	love.filesystem.write("upgrade_hp3.sold", 0)
	love.filesystem.write("upgrade_mp3.sold", 0)
	love.filesystem.write("upgrade_power3.sold", 0)
end
if not love.filesystem.exists("potion_a.upgrade") then
	love.filesystem.newFile("potion_a.upgrade")
	love.filesystem.newFile("potion_b.upgrade")
	love.filesystem.newFile("potion_c.upgrade")
	love.filesystem.newFile("potion_d.upgrade")
	love.filesystem.newFile("potion_e.upgrade")
	love.filesystem.newFile("potion_f.upgrade")
	love.filesystem.write("potion_a.upgrade", 0)
	love.filesystem.write("potion_b.upgrade", 0)
	love.filesystem.write("potion_c.upgrade", 0)
	love.filesystem.write("potion_d.upgrade", 0)
	love.filesystem.write("potion_e.upgrade", 0)
	love.filesystem.write("potion_f.upgrade", 0)
end
weapon = {
	name = "weapon",
	img = love.graphics.newImage("images/item/".. player.atrib .."/weapon.png"),
	hp = 0,
	mp = 0,
	power = 500,
	price = 1000,
	sold = love.filesystem.read("weapon.sold"),
	equip = false
}
armor = {
	name = "armor",
	img = love.graphics.newImage("images/item/".. player.atrib .."/armor.png"),
	hp = 1000,
	mp = 200,
	power = 0,
	price = 1000,
	sold = love.filesystem.read("armor.sold"),
	equip = false
}
gloves = {
	name = "gloves",
	img = love.graphics.newImage("images/item/".. player.atrib .."/gloves.png"),
	hp = 0,
	mp = 0,
	power = 20,
	price = 200,
	sold = love.filesystem.read("gloves.sold"),
	equip = false
}
belt = {
	name = "belt",
	img = love.graphics.newImage("images/item/".. player.atrib .."/belt.png"),
	hp = 100,
	mp = 10,
	power = 0,
	price = 100,
	sold = love.filesystem.read("belt.sold"),
	equip = false
}
braceles = {
	name = "braceles",
	img = love.graphics.newImage("images/item/".. player.atrib .."/braceles.png"),
	hp = 110,
	mp = 65,
	power = 10,
	price = 250,
	sold = love.filesystem.read("braceles.sold"),
	equip = false
}
pants = {
	name = "pants",
	img = love.graphics.newImage("images/item/".. player.atrib .."/pants.png"),
	hp = 800,
	mp = 400,
	power = 0,
	price = 700,
	sold = love.filesystem.read("pants.sold"),
	equip = false
}
boots = {
	name = "boots",
	img = love.graphics.newImage("images/item/".. player.atrib .."/boots.png"),
	hp = 0,
	mp = 0,
	power = 30,
	price = 400,
	sold = love.filesystem.read("boots.sold"),
	equip = false
}
hat = {
	name = "hat",
	img = love.graphics.newImage("images/item/".. player.atrib .."/hat.png"),
	hp = 0,
	mp = 140,
	power = 0,
	price = 200,
	sold = love.filesystem.read("hat.sold"),
	equip = false
}
potion_a = {
	name = "potion_a",
	img = love.graphics.newImage("images/item/potion/1.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_a.sold"),
	upgrade = love.filesystem.read("potion_a.upgrade"),
	equip = false
}
potion_b = {
	name = "potion_b",
	img = love.graphics.newImage("images/item/potion/2.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_b.sold"),
	upgrade = love.filesystem.read("potion_b.upgrade"),
	equip = false
}
potion_c = {
	name = "potion_c",
	img = love.graphics.newImage("images/item/potion/3.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_c.sold"),
	upgrade = love.filesystem.read("potion_c.upgrade"),
	equip = false
}
potion_d = {
	name = "potion_d",
	img = love.graphics.newImage("images/item/potion/4.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_d.sold"),
	upgrade = love.filesystem.read("potion_d.upgrade"),
	equip = false
}
potion_e = {
	name = "potion_e",
	img = love.graphics.newImage("images/item/potion/5.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_e.sold"),
	upgrade = love.filesystem.read("potion_e.upgrade"),
	equip = false
}
potion_f = {
	name = "potion_f",
	img = love.graphics.newImage("images/item/potion/6.png"),
	hp = 25,
	mp = 25,
	power = 25,
	price = 1000,
	sold = love.filesystem.read("potion_f.sold"),
	upgrade = love.filesystem.read("potion_f.upgrade"),
	equip = false
}
upgrade_hp = {
	name = "upgrade_hp",
	hp = 50,
	mp = 0,
	power = 0,
	luck = 3,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 500,
	sold = love.filesystem.read("upgrade_hp.sold"),
	equip = false
}
upgrade_mp = {
	name = "upgrade_mp",
	hp = 0,
	mp = 50,
	power = 0,
	luck = 3,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 500,
	sold = love.filesystem.read("upgrade_mp.sold"),
	equip = false
}
upgrade_power = {
	name = "upgrade_power",
	hp = 0,
	mp = 0,
	power = 50,
	luck = 3,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 500,
	sold = love.filesystem.read("upgrade_power.sold"),
	equip = false
}
upgrade_hp2 = {
	name = "upgrade_hp2",
	hp = 100,
	mp = 0,
	power = 0,
	luck = 6,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 1000,
	sold = love.filesystem.read("upgrade_hp2.sold"),
	equip = false
}
upgrade_mp2 = {
	name = "upgrade_mp2",
	hp = 0,
	mp = 100,
	power = 0,
	luck = 6,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 1000,
	sold = love.filesystem.read("upgrade_mp2.sold"),
	equip = false
}
upgrade_power2 = {
	name = "upgrade_power2",
	hp = 0,
	power = 100,
	mp = 0,
	luck = 6,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 1000,
	sold = love.filesystem.read("upgrade_power2.sold"),
	equip = false
}
upgrade_hp3 = {
	name = "upgrade_hp3",
	hp = 200,
	mp = 0,
	power = 0,
	luck = 9,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 2000,
	sold = love.filesystem.read("upgrade_hp3.sold"),
	equip = false
}
upgrade_mp3 = {
	name = "upgrade_mp3",
	hp = 0,
	mp = 200,
	power = 0,
	luck = 9,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 2000,
	sold = love.filesystem.read("upgrade_mp3.sold"),
	equip = false
}
upgrade_power3 = {
	name = "upgrade_power3",
	hp = 0,
	power = 200,
	mp = 0,
	luck = 9,
	img = love.graphics.newImage("images/upgrade.png"),
	price = 2000,
	sold = love.filesystem.read("upgrade_power3.sold"),
	equip = false
}
item_hp = 0
item_mp = 0
item_power = 0
function item_spawn(text, x, y)
	table.insert(item, {x = x, y = y, text = text, mouseover = false})
end
function item_draw()
	for i, v in ipairs(item) do
		if active == "Home" then
			if tonumber(v.text["sold"]) == 1  then
				if v.mouseover then
					love.graphics.setColor(255, 0, 0)
				else
					love.graphics.setColor(255, 255, 255)
				end

				if v.text["equip"] then
					love.graphics.printf("Equiped", v.x, v.y - 24, v.text["img"]:getWidth(), "center")
					love.graphics.rectangle("line", v.x - 1, v.y - 1, v.text["img"]:getWidth() + 2, v.text["img"]:getHeight() + 2)
				end

				love.graphics.draw(v.text["img"], v.x, v.y)
				love.graphics.print("HP = +"..v.text["hp"], v.x, v.y + 64)
				love.graphics.print("MP = +"..v.text["mp"], v.x, v.y + 79)
				love.graphics.print("DMG = +"..v.text["power"], v.x, v.y + 94)
			else
				love.graphics.setColor(0, 0, 255)
				love.graphics.draw(v.text["img"], v.x, v.y)
			end
		else
			if tonumber(v.text["sold"]) == 0 then	
				if v.mouseover then
					love.graphics.setColor(255, 0, 0)
				else
					love.graphics.setColor(255, 255, 255)
				end
				love.graphics.draw(v.text["img"], v.x, v.y)
				love.graphics.setColor(255, 255, 255)
				love.graphics.print("HP = +"..v.text["hp"], v.x, v.y + 64)
				love.graphics.print("MP = +"..v.text["mp"], v.x, v.y + 79)
				love.graphics.print("DMG = +"..v.text["power"], v.x, v.y + 94)
				love.graphics.print("$ = "..v.text["price"], v.x, v.y + 109)
			else
				if v.mouseover then
					love.graphics.setColor(0, 0, 255)
				else
					love.graphics.setColor(0, 255, 0)
				end
				love.graphics.draw(v.text["img"], v.x, v.y)
				love.graphics.setColor(255, 255, 255)
				love.graphics.print("HP = +"..v.text["hp"], v.x, v.y + 64)
				love.graphics.print("MP = +"..v.text["mp"], v.x, v.y + 79)
				love.graphics.print("DMG = +"..v.text["power"], v.x, v.y + 94)
				love.graphics.print("$ = ".."sold", v.x, v.y + 109)
			end
		end
	end
end
function item_click(x, y)
	for i, v in ipairs(item) do
		if x > v.x and x < v.x + 64 and y > v.y and y < v.y + 64 then
			if active == "Home" then		
				if tonumber(v.text["sold"]) == 1 then
					if not v.text["equip"] then
						item_hp = item_hp + v.text["hp"]
						item_mp = item_mp + v.text["mp"]
						item_power = item_power + v.text["power"]
						player.hp = original_hp * player.lvl + item_hp
						player.mp = original_mp * player.lvl + item_mp
						player.power = original_power * player.lvl + item_power
						v.text["equip"] = true
					elseif v.text["equip"] then
						item_hp = item_hp - v.text["hp"]
						item_mp = item_mp - v.text["mp"]
						item_power = item_power - v.text["power"]
						player.hp = original_hp * player.lvl + item_hp
						player.mp = original_mp * player.lvl + item_mp
						player.power = original_power * player.lvl + item_power
						v.text["equip"] = false
					end
				end
			else
				if tonumber(player.money) > v.text["price"] then
					v.text["sold"] = 1
					player.money = player.money - v.text["price"]
					love.filesystem.write(v.text["name"] .. ".sold", 1)
					if v.text == potion_a or v.text == potion_b or v.text == potion_c or v.text == potion_d or v.text == potion_e or v.text == potion_f then
						love.filesystem.write("boss_win", 0)
						boss_win = 0
					end
				end
			end
		end
	end
end
function item_check()
	for i, v in ipairs(item) do
		if mousex > v.x and mousex < v.x + 64 and mousey > v.y and mousey < v.y + 64 then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end

function item_clear()
	for k in pairs (item) do
		item [k] = nil
	end
end
