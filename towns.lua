towns = {}
if not love.filesystem.exists("Asqard.captured") then
	love.filesystem.newFile("Asqard.captured")
	love.filesystem.newFile("Taranth.captured")
	love.filesystem.newFile("Quintala.captured")
	love.filesystem.newFile("Asqard.wall")
	love.filesystem.newFile("Taranth.wall")
	love.filesystem.newFile("Quintala.wall")
	love.filesystem.newFile("Asqard.church")
	love.filesystem.newFile("Taranth.church")
	love.filesystem.newFile("Quintala.church")
	love.filesystem.newFile("Asqard.home")
	love.filesystem.newFile("Taranth.home")
	love.filesystem.newFile("Quintala.home")
	love.filesystem.newFile("Asqard.pub")
	love.filesystem.newFile("Taranth.pub")
	love.filesystem.newFile("Quintala.pub")
	love.filesystem.newFile("Asqard.player")
	love.filesystem.newFile("Taranth.player")
	love.filesystem.newFile("Quintala.player")
	love.filesystem.newFile("Asqard.shop")
	love.filesystem.newFile("Taranth.shop")
	love.filesystem.newFile("Quintala.shop")
	love.filesystem.write("Asqard.captured", 0)
	love.filesystem.write("Taranth.captured", 0)
	love.filesystem.write("Quintala.captured", 0)
	love.filesystem.write("Asqard.wall", 0)
	love.filesystem.write("Taranth.wall", 0)
	love.filesystem.write("Quintala.wall", 0)
	love.filesystem.write("Asqard.church", 0)
	love.filesystem.write("Taranth.church", 0)
	love.filesystem.write("Quintala.church", 0)
	love.filesystem.write("Asqard.home", 0)
	love.filesystem.write("Taranth.home", 0)
	love.filesystem.write("Quintala.home", 0)
	love.filesystem.write("Asqard.pub", 0)
	love.filesystem.write("Taranth.pub", 0)
	love.filesystem.write("Quintala.pub", 0)
	love.filesystem.write("Asqard.player", 0)
	love.filesystem.write("Taranth.player", 0)
	love.filesystem.write("Quintala.player", 0)
	love.filesystem.write("Asqard.shop", 0)
	love.filesystem.write("Taranth.shop", 0)
	love.filesystem.write("Quintala.shop", 0)
end
Asgard = {
	name ="Asqard",
	population = 10,
	army = 1,
	wall = love.filesystem.read("Asqard.wall"),
	church = love.filesystem.read("Asqard.church"),
	home = love.filesystem.read("Asqard.home"),
	pub = love.filesystem.read("Asqard.pub"),
	player = love.filesystem.read("Asqard.player"), 
	shop = love.filesystem.read("Asqard.shop"),
	captured = love.filesystem.read("Asqard.captured"),
	boss_HP = 800,
	boss_MP = 100,
	boss_POWER = 30,
	boss_IMG = love.graphics.newImage("images/boss.png")
}
Taranth = {
	name ="Taranth",
	population = 25,
	army = 10,
	wall = love.filesystem.read("Taranth.wall"),
	church = love.filesystem.read("Taranth.church"),
	home = love.filesystem.read("Taranth.home"),
	pub = love.filesystem.read("Taranth.pub"),
	player = love.filesystem.read("Taranth.player"), 
	shop = love.filesystem.read("Taranth.shop"),
	captured = love.filesystem.read("Taranth.captured"),
	boss_HP = 1500,
	boss_MP = 100,
	boss_POWER = 45,
	boss_IMG = love.graphics.newImage("images/boss.png")
}
Quintala = {
	name ="Quintala",
	population = 60,
	army = 1,
	wall = love.filesystem.read("Quintala.wall"),
	church = love.filesystem.read("Quintala.church"),
	home = love.filesystem.read("Quintala.home"),
	pub = love.filesystem.read("Quintala.pub"),
	player = love.filesystem.read("Quintala.player"), 
	shop = love.filesystem.read("Quintala.shop"),
	captured = love.filesystem.read("Quintala.captured"),
	boss_HP = 1900,
	boss_MP = 100,
	boss_POWER = 60,
	boss_IMG = love.graphics.newImage("images/boss.png")
}
asg_player = 0
asg_win = 6
function asgard()
	if town_active == "Build" then
		active_button_clear()
		active_button_spawn(awidth_min, aheight_min + 100, "Wall")
		active_button_spawn(awidth_min, aheight_min + 150, "Church")
		active_button_spawn(awidth_min , aheight_min + 200, "Home")
		active_button_spawn(awidth_min, aheight_min + 250, "Pub")
		active_button_spawn(awidth_min, aheight_min + 300, "Player")
		active_button_spawn(awidth_min , aheight_min + 350, "Shop")
		town_text = "You have now "..Asgard.population.." citizens\n and "..Asgard.army.." men ready to fight"
	elseif town_active == "Clear" then
		if asg_en then
			active_button_clear()
			if asg_clear then
				asg_num = math.random(2,Asgard.population)
				asg_clear = false
			end	
			town_text = "Around town are "..asg_num.." enemies and you have "..Asgard.army.." men ready to fight"
			active_button_spawn(awidth_max  - 100, aheight_max, "Get Them")
			asg_kill = true
		else
			town_text = "You need defeat hard enemy for get this"
		end
	else
		active_button_clear()
		active_button_spawn(awidth_min, aheight_min + 100, "Build")
		active_button_spawn(awidth_min, aheight_min + 150, "Clear")
		active_button_spawn(awidth_min , aheight_min + 200, "War")
		town_text = "This is now your town you can build some building \nor clean surrounding area \nor go to WAR"
		asg_clear = true
		--love.graphics.printf(town_text, awidth_mid, aheight_min + 100, awidth_max - awidth_mid, 'left')
	end
	if town_active == "War" then
		town_text = "Comming soon"
	end
	if town_active == "Get Them" then
		active_button_clear()
		if asg_kill then
			asg_player = asg_num - math.random(1, 3) * Asgard.army
			asg_kill = false
			--print(asg_player)
		end
		--print(asg_player)
		town_text = "Your army kill lot of them but now you must kill "..asg_player
		active_button_spawn(awidth_max  - 100, aheight_max, "Go Fight")
		if asg_player == 1 then
			enemy.hp = enemy_hp * player.lvl * 4
			enemy.mp = enemy_mp * player.lvl
			enemy.power = enemy_power * player.lvl * 2
			enemy.xp = 3 * player.lvl
			enemy.img = easy[ math.random( #easy ) ]
			asg_win = 1
		end
		if asg_player == 2 then
			enemy.hp = enemy_hp * player.lvl * 4.5
			enemy.mp = enemy_mp * player.lvl * 0.8
			enemy.power = enemy_power * player.lvl * 2.1
			enemy.xp = 4 * player.lvl
			enemy.img = medium[ math.random( #medium ) ]
			asg_win = 2
		end
		if asg_player == 3 then
			enemy.hp = enemy_hp * player.lvl * 5
			enemy.mp = enemy_mp * player.lvl * 0.4
			enemy.power = enemy_power * player.lvl * 2.2
			enemy.xp = 5 * player.lvl
			enemy.img = hard[ math.random( #hard ) ]
			asg_win = 3
		end
		if asg_player == 4 then
			enemy.hp = enemy_hp * player.lvl * 5.5
			enemy.mp = enemy_mp * player.lvl * 0.6
			enemy.power = enemy_power * player.lvl * 2.3
			enemy.xp = 6 * player.lvl
			enemy.img = hard[ math.random( #hard ) ]
			asg_win = 4
		end
		if asg_player > 4 then
			enemy.hp = enemy_hp * player.lvl * 6
			enemy.mp = enemy_mp * player.lvl * 0.4
			enemy.power = enemy_power * player.lvl * 2.4
			enemy.xp = 7 * player.lvl
			enemy.img = hard[ math.random( #hard ) ]
			asg_win = 5
		end
		if asg_player < 1 then
			asg_en = false
			town_active = "Clear"
		--	win = true
		--	--asg_win = 0
		end
		
	end
	love.graphics.printf(town_text, awidth_mid, aheight_min + 100, awidth_max - awidth_mid, 'left')
end
function asgard_boss()
	town_text = "You need kill town boss to capture town"
	enemy.hp = Asgard.boss_HP * player.lvl
	enemy.mp = Asgard.boss_MP * player.lvl
	enemy.power = Asgard.boss_POWER * player.lvl
	enemy.img = Asgard.boss_IMG
	asg_boss = true
	active_button_spawn(awidth_max  - 100, aheight_max, "Capture")
	love.graphics.printf(town_text, awidth_mid, aheight_min + 100, awidth_max - awidth_mid, 'left')
	
end
